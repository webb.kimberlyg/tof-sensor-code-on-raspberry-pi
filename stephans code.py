# import the module
from ctypes import cdll

# load the library
lib = cdll.LoadLibrary('./libRange.so')

# create a Geek class
class WaterLvl(object):

	# constructor
	def __init__(self):

		# attribute
		self.obj = lib.WaterLvl_new()

	# define method
	def myFunction(self):
		return lib.WaterLvl_myFunction(self.obj)
		

# create a Geek class object
f = WaterLvl()

# object method calling
print(f.myFunction())

