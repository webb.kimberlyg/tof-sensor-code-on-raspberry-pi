import subprocess
import time
import datetime;
import socket,json,http.client,urllib.request,urllib.parse,urllib.error

connection = http.client.HTTPSConnection('parseapi.back4app.com', 443)

params = urllib.parse.urlencode({"excludeKeys":"playerName,createdAt,updatedAt"})

connection.connect()

def createEntry(waterLvl,dateTime):
    # --------------------------------- CREATE ENTRY IN DATABASE -----------------------------------
     connection.request('POST', '/parse/classes/serverData', json.dumps({
            "Location": "Hagley Park Road",
            "waterLevel": waterLvl,
            "Date":dateTime
          }), {
            "X-Parse-Application-Id": "ogolVwFoGy0tKQ9INJaRJLGUBlWxbTeKFBibUwOX",
            "X-Parse-REST-API-Key": "M5tDdkGMIb7KX1xCNPmITudauaHBfiLYxA225UQ7",
            "Content-Type": "application/json"
          })
     results = json.loads(connection.getresponse().read())
     print(results)

def updateDatabase(waterLvl,dateTime):
    # --------------------------------- UPDATE DATABASE -----------------------------------

     connection.request('PUT', '/parse/classes/serverData/tNPKnctQB0', json.dumps({
            "waterLevel": waterLvl,
            "Date":dateTime
          }), {
            "X-Parse-Application-Id": "ogolVwFoGy0tKQ9INJaRJLGUBlWxbTeKFBibUwOX",
            "X-Parse-REST-API-Key": "M5tDdkGMIb7KX1xCNPmITudauaHBfiLYxA225UQ7",
            "Content-Type": "application/json"
         })
     result = json.loads(connection.getresponse().read())
     print(result)

def retrieveData():
    # ------------------------- FEWS APP - RETRIEVE DATA -----------------------------
     connection.request('GET', '/parse/classes/serverData?%s' % params, '', {
            "X-Parse-Application-Id": "ogolVwFoGy0tKQ9INJaRJLGUBlWxbTeKFBibUwOX",
            "X-Parse-REST-API-Key": "M5tDdkGMIb7KX1xCNPmITudauaHBfiLYxA225UQ7"
          })
     result = json.loads(connection.getresponse().read())
     print(json.dumps(result, indent=2))
    
    
while 1:
    
    output = subprocess.run(["./Range", "a.in", "0.001"], stdout = subprocess.PIPE,
                        universal_newlines = True).stdout
    ct = datetime.datetime.now()
    int_output=int(output)
    string_dateTime=str(ct)
    print("current time: ", string_dateTime)
    print(int_output)
    
    createEntry(int_output,string_dateTime)
    #print(type(int_output))
    time.sleep(120)
    